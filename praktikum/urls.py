from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import lab_2_addon.urls as lab_2_addon
import lab_1.urls as lab_1
import lab_2.urls as lab_2
import lab_3.urls as lab_3
import lab_4.urls as lab_4
import lab_5.urls as lab_5
from lab_1.views import index as index_lab1


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^lab-1/', include(lab_1,namespace='lab-1')),
    url(r'^lab-2/', include(lab_2,namespace='lab-2')),
    url(r'^lab-2-addon/', include(lab_2_addon,namespace='lab-2-addon')),
    url(r'^lab-3/', include(lab_3,namespace='lab-3')),
    url(r'^$', RedirectView.as_view(url="/lab-4/", permanent="true"), name='index'),
    url(r'^lab-4/', include(lab_4, namespace='lab-4')),
    url(r'^lab-5/', include(lab_5, namespace='lab-5'))
]
